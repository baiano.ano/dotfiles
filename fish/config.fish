if status is-interactive
    # Commands to run in interactive sessions can go here
end

alias n="nvim"
alias ls="exa --color=always"
starship init fish | source
export EDITOR="/usr/bin/nvim"
export VISUAL="/usr/bin/nvim"
export BW_SESSION="SheIVvLF4FkUt+CC9fVbB3UZlysDp10PTXM8cDhSzLbhsw5XNGMoRcQLLjWSsbBVeoz3tYqIJo3mGGGtqSS7Ug=="
function fish_greeting
  fastfetch
end
