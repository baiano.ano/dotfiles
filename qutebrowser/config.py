# import catppuccin

##### Catppuccin themes and qutebrowser managing it
import os
from urllib.request import urlopen

# load your autoconfig, use this, if the rest of your config is empty!
config.load_autoconfig(False)

if not os.path.exists(config.configdir / "theme.py"):
    theme = "https://raw.githubusercontent.com/catppuccin/qutebrowser/main/setup.py"
    with urlopen(theme) as themehtml:
        with open(config.configdir / "theme.py", "a") as file:
            file.writelines(themehtml.read().decode("utf-8"))

if os.path.exists(config.configdir / "theme.py"):
    import theme
    theme.setup(c, 'frappe', True)


config.set("fileselect.handler", "external")
config.set(
    "fileselect.single_file.command",
    ["kitty", "--class", "yazi,yazi", "-e", "yazi", "--chooser-file", "{}"],
)
config.set(
    "fileselect.multiple_files.command",
    ["kitty", "--class", "yazi,yazi", "-e", "yazi", "--chooser-file", "{}"],
)


# config.set("colors.webpage.darkmode.enabled", True)
# config.set("colors.webpage.darkmode.algorithm", "brightness-rgb")
# config.set("colors.webpage.darkmode.grayscale.all", False)
# config.set("colors.webpage.darkmode.policy.images", "never")

config.set("keyhint.delay", 0)

config.set("scrolling.bar", "never")

# Themes --> Catppuccin

config.set(
    "content.headers.user_agent",
    "Mozilla/5.0 ({os_info}) AppleWebKit/{webkit_version} (KHTML, like Gecko) {qt_key}/{qt_version} {upstream_browser_key}/{upstream_browser_version} Safari/{webkit_version}",
)
config.bind(
    "ç",
    "message-info 'Toggling Linux/Default';; config-cycle content.headers.user_agent 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36' 'Mozilla/5.0 ({os_info}) AppleWebKit/{webkit_version} (KHTML, like Gecko) {qt_key}/{qt_version} {upstream_browser_key}/{upstream_browser_version} Safari/{webkit_version}'",
)

config.set("url.searchengines", {"DEFAULT": "https://search.brave.com/search?q={}"})
config.set("url.default_page", "https://web.tabliss.io/")

config.set("url.start_pages", "https://web.tabliss.io/")
config.set("content.autoplay", False)
config.set("tabs.show", "multiple")
config.set("statusbar.show", "in-mode")
config.set("hints.chars", "asdfhjkl")
config.set("editor.command", ["nvim", "-i", "{file}", "-c", "normal {line}G{column0}l"])

############################ BINDS ####################################
config.bind(",m", "spawn mpv {url}")
config.bind(",M", "hint links spawn mpv {hint-url}")
config.set("scrolling.smooth", True)
config.set("zoom.default", 100)
# config.set("tabs.show_switching_delay", 1000)
config.set("zoom.mouse_divider", 1024)
config.set("colors.webpage.preferred_color_scheme", "light")
config.set("content.cookies.accept", "no-3rdparty")
config.set("tabs.position", "top")



################## Qutebrowser Bitwarden Userscript ###########################



config.bind('zl', 'spawn --userscript qute-bitwarden')
