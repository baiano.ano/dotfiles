# #######################################################################################
# AUTOGENERATED HYPR CONFIG.
# PLEASE USE THE CONFIG PROVIDED IN THE GIT REPO /examples/hypr.conf AND EDIT IT,
# OR EDIT THIS ONE ACCORDING TO THE WIKI INSTRUCTIONS.
# #######################################################################################

#
# Please note not all available settings / options are set here.
# For a full list, see the wiki
#
source = ~/.config/hypr/mocha.conf


exec-once = swaync
exec-once = mullvad-daemon & mullvad-vpn
exec-once = waybar
exec-once = blueman-applet & nm-applet
exec-once = filen
exec-once = wlsunset -t 3100 -T 4000

# Clipboard manager
exec-once = clipman restore
exec-once = wl-paste -t text --watch clipman store


# See https://wiki.hyprland.org/Configuring/Monitors/
monitor=,preferred,auto,auto


# See https://wiki.hyprland.org/Configuring/Keywords/ for more

# Execute your favorite apps at launch
# exec-once = waybar & hyprpaper & firefox

# Source a file (multi-file configs)
# source = ~/.config/hypr/myColors.conf

# Set programs that you use
$terminal = kitty
$fileManager = thunar
$menu = tofi-drun
$email = tutanota
$browser = firefox

# Some default env vars.
exec-once = hyprctl setcursor Catppuccin-Mocha-Peach-Cursors 24
env = XCURSOR_SIZE,24
env = QT_QPA_PLATFORMTHEME,qt5ct # change to qt6ct if you have that

# For all categories, see https://wiki.hyprland.org/Configuring/Variables/
input {
    kb_layout = br
    numlock_by_default = true
    follow_mouse = 1

    touchpad {
        natural_scroll = yes
    }

    sensitivity = 0.8 # -1.0 to 1.0, 0 means no modification.
    accel_profile = flat
}

general {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more

    gaps_in = 5
    gaps_out = 10
    border_size = 2
    col.active_border = $mauve $pink 45deg
    col.inactive_border = rgba(595959aa)

    layout = dwindle

    # Please see https://wiki.hyprland.org/Configuring/Tearing/ before you turn this on
    allow_tearing = false
}

decoration {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more

    rounding = 10
    
    blur {
        enabled = false
        # size = 3
        # passes = 1
    }

    drop_shadow = false # uncomment the following lines if this one is true
    # shadow_range = 4
    # shadow_render_power = 3
    # col.shadow = rgba(1a1a1aee)
}

animations {
    enabled = yes

    # Some default animations, see https://wiki.hyprland.org/Configuring/Animations/ for more

    bezier = myBezier, 0.05, 0.9, 0.1, 1.05

    animation = windows, 1, 4, myBezier
    animation = windowsOut, 1, 4, default, popin 80%
    animation = border, 1, 10, default
    animation = borderangle, 1, 8, default
    animation = fade, 1, 4, default
    animation = workspaces, 1, 4, default
}

dwindle {
    # See https://wiki.hyprland.org/Configuring/Dwindle-Layout/ for more
    pseudotile = yes # master switch for pseudotiling. Enabling is bound to mainMod + P in the keybinds section below
    preserve_split = yes # you probably want this
}

master {
    # See https://wiki.hyprland.org/Configuring/Master-Layout/ for more
    new_is_master = true
}

gestures {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more
    workspace_swipe = on
}

misc {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more
    force_default_wallpaper = 0 # Set to 0 or 1 to disable the anime mascot wallpapers
    vfr = true
}

# Example per-device config
# See https://wiki.hyprland.org/Configuring/Keywords/#per-device-input-configs for more
device {
    name = logitech-g403-hero-gaming-mouse
    accel_profile = flat
    sensitivity = 0
}

windowrulev2 = workspace 1 silent, class:^(firefox)$
windowrulev2 = workspace 1 silent, class:^(org.qutebrowser.qutebrowser)$
windowrulev2 = workspace 3 silent, class:^(org.pwmt.zathura)$
windowrulev2 = workspace 4 silent, class:^(Beeper)$
windowrulev2 = workspace 4 silent, class:^(discord)$
windowrulev2 = workspace 5 silent, class:^(com.stremio.stremio)$
windowrulev2 = workspace 5 silent, class:^(tutanota-desktop)$
windowrulev2 = workspace 5 silent, class:^(YouTube Music)$
windowrulev2 = workspace 6 silent, class:^(Bitwarden)$
windowrulev2 = workspace 6 silent, class:^(org.mozilla.Thunderbird)$
windowrulev2 = workspace 7 silent, class:^(steam)$
windowrulev2 = float, class:^(org.gnome.Calculator)$

# Example windowrule v2
# See https://wiki.hyprland.org/Configuring/Window-Rules/ for more
windowrulev2 = suppressevent maximize, class:.* # You'll probably like this.

# See https://wiki.hyprland.org/Configuring/Keywords/ for more
$mainMod = SUPER

# Example binds, see https://wiki.hyprland.org/Configuring/Binds/ for more
bind = $mainMod, RETURN, exec, $terminal
bind = $mainMod, Q, killactive, 
bind = $mainMod, Escape, exec, ~/.config/hypr/hkill.sh
bind = $mainMod, M, exec, wlogout 
bind = $mainMod, V, togglefloating, 
bind = $mainMod, D, exec, $menu
bind = $mainMod, P, pseudo, # dwindle
bind = $mainMod, T, togglesplit, # dwindle
bind = CTRL ALT, L, exec, hyprlock
bind = $mainMod, F, fullscreen
bind = CTRL ALT, Q, exec, qutebrowser
bind = CTRL ALT, F, exec, $browser
bind = $mainMod, E, exec, $fileManager
bind = CTRL ALT, T, exec, $email
bind = CTRL ALT, Y, exec, kitty -e yazi
bind = CTRL ALT, C, exec, gnome-calculator
bind = CTRL ALT, D, exec, discord

bind = $mainMod, N, exec, swaync-client -t

# Set the media keys of my laptop
# Brightness
bindel =, XF86_MonBrightnessUp, exec, brightnessctl s +5%
bindel =, XF86_MonBrightnessDown, exec, brightnessctl s 5%- 

# Volume
bindel =, XF86_AudioRaiseVolume, exec, wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%+ # this is actualy from the hypr wiki 
bindel =, XF86_AudioLowerVolume, exec, wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%- # this is actualy from the hypr wiki 
bindel =, XF86_AudioMute, exec, wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle # this is actualy from the hypr wiki 
bindel = , XF86_AudioMicMute, exec, wpctl set-mute @DEFAULT_AUDIO_SOURCE@ toggle

# Lock screen
bind =, XF86_ModeLock, exec, hyprlock

# Media
bindel =, XF86_AudioPlay, exec, playerctl play-pause
bindel =, XF86_AudioNext, exec, playerctl next
bindel =, XF86_AudioPrev, exec, playerctl previous
bindel =, XF86_AudioStop, exec, playerctl stop

# Printscreen
bind =, Print, exec, grim
bind = CTRL, Print, exec, grim -g "$(slurp)"

# Move focus with mainMod + vim-like keys
bind = $mainMod, H, movefocus, l
bind = $mainMod, L, movefocus, r
bind = $mainMod, K, movefocus, u
bind = $mainMod, J, movefocus, d

# Move focused window with vim-like keys
bind = $mainMod SHIFT, H, movewindow, l
bind = $mainMod SHIFT, L, movewindow, r
bind = $mainMod SHIFT, K, movewindow, u
bind = $mainMod SHIFT, J, movewindow, d

# Move window to next or previous workspace
bind = $mainMod SHIFT, comma, movetoworkspace, e+1
bind = $mainMod SHIFT, period, movetoworkspace, e-1

# Switch workspaces with mainMod + [0-9]
bind = $mainMod, 1, workspace, 1
bind = $mainMod, 2, workspace, 2
bind = $mainMod, 3, workspace, 3
bind = $mainMod, 4, workspace, 4
bind = $mainMod, 5, workspace, 5
bind = $mainMod, 6, workspace, 6
bind = $mainMod, 7, workspace, 7
bind = $mainMod, 8, workspace, 8
bind = $mainMod, 9, workspace, 9
bind = $mainMod, 0, workspace, 10

# Move active window to a workspace with mainMod + SHIFT + [0-9]
bind = $mainMod SHIFT, 1, movetoworkspacesilent, 1
bind = $mainMod SHIFT, 2, movetoworkspacesilent, 2
bind = $mainMod SHIFT, 3, movetoworkspacesilent, 3
bind = $mainMod SHIFT, 4, movetoworkspacesilent, 4
bind = $mainMod SHIFT, 5, movetoworkspacesilent, 5
bind = $mainMod SHIFT, 6, movetoworkspacesilent, 6
bind = $mainMod SHIFT, 7, movetoworkspacesilent, 7
bind = $mainMod SHIFT, 8, movetoworkspacesilent, 8
bind = $mainMod SHIFT, 9, movetoworkspacesilent, 9
bind = $mainMod SHIFT, 0, movetoworkspacesilent, 10

# Example special workspace (scratchpad)
bind = $mainMod, S, togglespecialworkspace, magic
bind = $mainMod SHIFT, S, movetoworkspace, special:magic

# Scroll through existing workspaces with mainMod + ,.
bind = $mainMod, mouse_up, workspace, e+1
bind = $mainMod, mouse_down, workspace, e-1

bind = $mainMod, comma, workspace, e+1
bind = $mainMod, period, workspace, e-1

# Move/resize windows with mainMod + LMB/RMB and dragging
bindm = $mainMod, mouse:272, movewindow
bindm = $mainMod, mouse:273, resizewindow

